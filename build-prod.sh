#!/usr/bin/env bash
docker build --rm -f ./Dockerfile.prod -t subaruqui/bms-www .
on_build=$?
if [ $on_build -eq 0 ]; then
    docker push subaruqui/bms-www
fi

