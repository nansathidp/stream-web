#!/usr/bin/env bash
TAG=$1
docker build --rm  -f ./Dockerfile.$TAG -t subaruqui/bms-www:$TAG .
BUILD=$?

if [ $BUILD -eq 0 ]; then
    docker push subaruqui/bms-www:$TAG
fi
