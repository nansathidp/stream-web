FROM node:12.1-slim as build-stage
RUN apt update -y && apt install g++  build-essential python xsel -y

RUN npm install -g serve
WORKDIR /app
COPY package*.json ./
RUN npm install
RUN npm install -g serve
COPY ./ .
RUN npm install -g @vue/cli@latest
ENV NODE_ENV=staging
RUN npm run build
CMD serve -s dist

