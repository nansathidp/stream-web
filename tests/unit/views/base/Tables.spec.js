import Vue from 'vue'
// import { mount, shallowMount } from '@vue/test-utils';
import { mount } from '@vue/test-utils';
import BootstrapVue from 'bootstrap-vue'
import ScrollingText from '@/views/scrolling/ScrollingText'

Vue.use(BootstrapVue)

describe('ScrollingText.vue', () => {
  it('has a name', () => {
    expect(ScrollingText.name).toMatch('scrollingText')
  })
  it('is Vue instance', () => {
    const wrapper = mount(ScrollingText)
    expect(wrapper.isVueInstance()).toBe(true)
  })
  it('is ScrollingText', () => {
    const wrapper = mount(ScrollingText)
    expect(wrapper.is(ScrollingText)).toBe(true)
  })
  it('should render correct content', () => {
    const wrapper = mount(ScrollingText)
    expect(wrapper.find('div.card-header > div').text()).toMatch('Simple ScrollingText')
  })
  // test('renders correctly', () => {
  //   const wrapper = shallowMount(Tables)
  //   expect(wrapper.element).toMatchSnapshot()
  // })
})
