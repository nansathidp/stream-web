import Vue from 'vue'
import { shallowMount, mount } from '@vue/test-utils'
import BootstrapVue from 'bootstrap-vue'
import ScrollingConfig from '@/views/scrolling/ScrollingConfig'

Vue.use(BootstrapVue)

describe('ScrollingConfig.vue', () => {
  it('has a name', () => {
    expect(ScrollingConfig.name).toMatch('scrolling-config')
  })
  it('is Vue instance', () => {
    const wrapper = shallowMount(ScrollingConfig)
    expect(wrapper.isVueInstance()).toBe(true)
  })
  it('is ScrollingConfig', () => {
    const wrapper = shallowMount(ScrollingConfig)
    expect(wrapper.is(ScrollingConfig)).toBe(true)
  })
  it('should render correct content', () => {
    const wrapper = mount(ScrollingConfig)
    expect(wrapper.find('header.card-header > div > strong').text()).toMatch('Bootstrap list group')
  })
  test('renders correctly', () => {
    const wrapper = shallowMount(ScrollingConfig)
    expect(wrapper.element).toMatchSnapshot()
  })
})
