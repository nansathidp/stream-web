#!/usr/bin/env bash
docker build --rm -f ./Dockerfile.prepod -t subaruqui/bms-www:prepod .
on_build=$?
if [ $on_build -eq 0 ]; then
    docker push subaruqui/bms-www:prepod
fi

