export default {
  data: () => ({
    process: {
      complete: 'success',
      transfer: 'warning',
      transcoding: 'warning'
    },
    status: {
      process: 'warning',
      ready: 'warning',
      complete: 'success',
      receive: 'success'
    },
    label_lang: {
      tha: 'primary',
      eng: 'success',
      kor: 'info',
      chi: 'danger',
      jpn: 'warning',
      ind: 'secondary',
      und: 'dark'
    },
    contents: [],
  }),

  methods: {
    resetContent() {
      this.content = {
        // id: -1,
        name_en: '',
        name_th: '',
        synopsis_en: '',
        synopsis_th: '',
        content_no: 1,
        duration: '000000',
        total_episode: 1
      }
    },
    validContent() {
      return this.content.name_en && this.content.name_th
    },
    addContentList(isReset = true, content = {}) {
      if (this.validContent()) {
        if (Object.keys(content).length > 0)
          this.contents.push(content);
        else
          this.contents.push(this.content)

      }
      if (isReset) {
        this.resetContent()
      }
    },
  }
}
