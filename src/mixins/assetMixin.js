export default {
    data: () => ({
        status: {
            Running: 'deepskyblue',
            Completed: 'lawngreen',
            Failed: '#f86c6b',
            Timed_out: 'sandybrown',
            Terminated: 'grey',
            Paused: 'lightgrey',
            Waiting: 'yellow',
            none: '#ffffff'
        },
    })
}