export default {
  data: () => ({
    permission: {},
    userPermissions: [],
    groupPermission: []
  }),
  async create() {
    try {
      this.userPermissions = await this.getUserPermission()
      await this.fetchPermission()
      await this.fetchGroupPermission(this.permission.id)
    } catch (error) {
      this.errorHandle(error)
    }
  },
  methods: {
    async getUserPermission() {
      return await this.$services.profile.getPermission()
    },
    errorHandle(error) {
      if (error.response.status === 401) {
        location.replace('/pages/401')
      }
    },

    async fetchPermission() {
      try {
        await this.$services.profile.findByToken(localStorage.token).then(
          response => (
            this.permission = response.data
          )
        )
      } catch (e) {
        this.errorHandle(e)
      }
    },
    async initialPermission(app, moduleApp) {

      let userPermissions = this.$store.state.userUserPermission
      if (userPermissions.length === 0){
        userPermissions = await this.getUserPermission()
        this.$store.dispatch('initPermission', userPermissions)
      }
      const codename = `${app}`
      return {
        isView: userPermissions.includes(`${codename}.view_${moduleApp}`),
        isDetail: userPermissions.includes(`${codename}.view_detail_${moduleApp}`),
        isAdd: userPermissions.includes(`${codename}.add_${moduleApp}`),
        isEdit: userPermissions.includes(`${codename}.change_${moduleApp}`),
        isDelete: userPermissions.includes(`${codename}.delete_${moduleApp}`),
        isDisable: ! (userPermissions.includes(`${codename}.add_${moduleApp}`) ||  userPermissions.includes(`${codename}.change_${moduleApp}`)),
        isExport: userPermissions.includes(`${codename}.export_${moduleApp}`),
        isImport: userPermissions.includes(`${codename}.import_${moduleApp}`) 
      }
    },
    async fetchGroupPermission(id) {
      // try {
      //   await this.$services.profile.findGroupById(id).then(
      //     response => (
      //       this.groupPermission = response.data
      //     )
      //   )
      // } catch (error) {
      //   this.errorHandle(error)
      // }
    }
  }
}
