import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex);
export const storeSchedulePublish = new Vuex.Store({
  state: {
    schedule_publish: {
      title: '',
      total_episode: '',
      start_pac_no: '',
      end_pac_no: '',
      start: '',
      end: '',
      day: [],
      day_type: '',
      publish_zone: [],
    }
  },

  mutations: {
    setSchedulePublishTitle(state, title) {
      state.schedule_publish.title = title
    },
    setSchedulePublishTotalEpisode(state, total_episode) {
      state.schedule_publish.total_episode = total_episode
    },
    setSchedulePublishStartPacNo(state, start_pac_no) {
      state.schedule_publish.start_pac_no = start_pac_no
    },
    setSchedulePublishEndPacNo(state, end_pac_no) {
      state.schedule_publish.end_pac_no = end_pac_no
    },
    setSchedulePublishStart(state, start) {
      state.schedule_publish.start = start
    },
    setSchedulePublishEnd(state, end) {
      state.schedule_publish.end = end
    },
    setSchedulePublishDay(state, day) {
      state.schedule_publish.day = day
    },
    setSchedulePublishDayType(state, day_type) {
      state.schedule_publish.day_type = day_type
    },
    setSchedulePublishPublishZone(state, publish_zone) {
      state.schedule_publish.publish_zone = publish_zone
    },
  },

  actions: {
    toSchedulePublishTitle({commit, state}, title) {
      commit('setSchedulePublishTitle', title);
      state.schedule_publish.title = title
    },
    toSchedulePublishTotalEpisode({commit, state}, total_episode) {
      commit('setSchedulePublishTotalEpisode', total_episode);
      state.schedule_publish.total_episode = total_episode
    },
    toSchedulePublishStartPacNo({commit, state}, start_pac_no) {
      commit('setSchedulePublishStartPacNo', start_pac_no);
      state.schedule_publish.start_pac_no = start_pac_no
    },
    toSchedulePublishEndPacNo({commit, state}, end_pac_no) {
      commit('setSchedulePublishEndPacNo', end_pac_no);
      state.schedule_publish.end_pac_no = end_pac_no
    },
    toSchedulePublishStart({commit, state}, start) {
      commit('setSchedulePublishStart', start);
      state.schedule_publish.start = start
    },
    toSchedulePublishEnd({commit, state}, end) {
      commit('setSchedulePublishEnd', end);
      state.schedule_publish.end = end
    },
    toSchedulePublishDay({commit, state}, day) {
      commit('setSchedulePublishDay', day);
      state.schedule_publish.day = day
    },
    toSchedulePublishDayType({commit, state}, day_type) {
      commit('setSchedulePublishDayType', day_type);
      state.schedule_publish.day_type = day_type
    },
    toSchedulePublishPublishZone({commit, state}, publish_zone) {
      commit('setSchedulePublishPublishZone', publish_zone);
      state.schedule_publish.publish_zone = publish_zone
    },
  },
  getters: {
    title: state => state.schedule_publish.title,
    total_episode: state => state.schedule_publish.total_episode,
    start_pac_no: state => state.schedule_publish.start_pac_no,
    end_pac_no: state => state.schedule_publish.end_pac_no,
    start: state => state.schedule_publish.start,
    end: state => state.schedule_publish.end,
    day: state => state.schedule_publish.day,
    day_type: state => state.schedule_publish.day_type,
    publish_zone: state => state.schedule_publish.publish_zone,
  }
});
