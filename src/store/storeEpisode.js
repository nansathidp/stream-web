import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex);
export const storeEpisode = new Vuex.Store({
  state: {
    episode: {
      episode_id: '',
      episode_name: '',
      pac_no: '',
      duration: '',
      use_run: 0,
      aka: [],
      publish: [],
      segment: [],
    }
  },

  mutations: {
    setEpisodeEpisodeId(state, episode_id) {
      state.episode.episode_id = episode_id
    },
    setEpisodeEpisodeName(state, episode_name) {
      state.episode.episode_name = episode_name
    },
    setEpisodePacNo(state, pac_no) {
      state.episode.pac_no = pac_no
    },
    setEpisodeDuration(state, duration) {
      state.episode.duration = duration
    },
    setEpisodeUseRun(state, use_run) {
      state.episode.use_run = use_run
    },
    setEpisodeAkaSynopsis(state, aka) {
      state.episode.aka = aka
    },
    setEpisodePublish(state, publish) {
      state.episode.publish = publish
    },
    setEpisodeSegment(state, segment) {
      state.episode.segment = segment
    },
  },
  actions: {
    toEpisodeEpisodeId({commit, state}, episode_id) {
      commit('setEpisodeEpisodeId', episode_id);
      state.episode.episode_id = episode_id
    },
    toEpisodeEpisodeName({commit, state}, episode_name) {
      commit('setEpisodeEpisodeName', episode_name);
      state.episode.episode_name = episode_name
    },
    toEpisodePacNo({commit, state}, pac_no) {
      commit('setEpisodePacNo', pac_no);
      state.episode.pac_no = pac_no
    },
    toEpisodeDuration({commit, state}, duration) {
      commit('setEpisodeDuration', duration);
      state.episode.duration = duration
    },
    toEpisodeUseRun({commit, state}, use_run) {
      commit('setEpisodeUseRun', use_run);
      state.episode.use_run = use_run
    },
    toEpisodeAkaSynopsis({commit, state}, aka) {
      commit('setEpisodeAkaSynopsis', aka);
      state.episode.aka = aka
    },
    toEpisodePublish({commit, state}, publish) {
      commit('setEpisodePublish', publish);
      state.episode.publish = publish
    },
    toEpisodeSegment({commit, state}, segment) {
      commit('setEpisodeSegment', segment);
      state.episode.segment = segment
    },
  },
  getters: {
    episode_id: state => state.episode.episode_id,
    episode_name: state => state.episode.episode_name,
    pac_no: state => state.episode.pac_no,
    duration: state => state.episode.duration,
    use_run: state => state.episode.use_run,
    aka: state => state.episode.aka,
    publish: state => state.episode.publish,
    segment: state => state.episode.segment,
  }
});
