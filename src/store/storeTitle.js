import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex);
export const storeTitle = new Vuex.Store({
  state: {
    title: {
      id: '',
      name: '',
      vendor_id: '',
      studio_id: '',
      status: '',
      title_type: '',
      total_of_episode: 1,
      rating: 0,
      country: '',
      release_year: '',
      genre: '',
      duration: '',
      cost: 0,
      right_list: [],
      episode_list: [],
      aka_synopsis_list: [],
      cast_list: [],
      thumbnail_list: [],
      title_cost_list: [],
      localize_list: [],
    },
  },

  mutations: {
    setTitleId(state, id) {
      state.title.id = id
    },
    setTitleTitleName(state, name) {
      state.title.name = name
    },
    setTitleVendor(state, vendor_id) {
      state.title.vendor_id = vendor_id
    },
    setTitleStudio(state, studio_id) {
      state.title.studio_id = studio_id
    },
    setTitleStatus(state, status) {
      state.title.status = status
    },
    setTitleTitleType(state, title_type) {
      state.title.title_type = title_type
    },
    setTitleTotalOfEpisode(state, total_of_episode) {
      state.title.total_of_episode = total_of_episode
    },
    setTitleRating(state, rating) {
      state.title.rating = rating
    },
    setTitleCountry(state, country) {
      state.title.country = country
    },
    setTitleReleaseYear(state, release_year) {
      state.title.release_year = release_year
    },
    setTitleGenre(state, genre) {
      state.title.genre = genre
    },
    setTitleDuration(state, duration) {
      state.title.duration = duration
    },
    setTitleTitleCost(state, title_cost_list) {
      state.title.title_cost_list = title_cost_list
    },
    setTitleRight(state, right_list) {
      state.title.right_list = right_list
    },
    setTitleEpisode(state, episode_list) {
      state.title.episode_list = episode_list
    },
    setTitleAkaSynopsis(state, aka_synopsis_list) {
      state.title.aka_synopsis_list = aka_synopsis_list
    },
    setTitleCast(state, cast_list) {
      state.title.cast_list = cast_list
    },
    setTitleThumbnail(state, thumbnail_list) {
      state.title.thumbnail_list = thumbnail_list
    },
    setTitleCost(state, cost) {
      state.title.cost = cost
    },
    setTitleLocalize(state, localize_list) {
      state.title.localize_list = localize_list
    },
  },

  actions: {
    toTitleId({commit, state}, id) {
      commit('setTitleId', id);
      state.title.id = id
    },
    toTitleTitleName({commit, state}, name) {
      commit('setTitleTitleName', name);
      state.title.name = name
    },
    toTitleVendor({commit, state}, vendor_id) {
      commit('setTitleVendor', vendor_id);
      state.title.vendor_id = vendor_id
    },
    toTitleStudio({commit, state}, studio_id) {
      commit('setTitleStudio', studio_id);
      state.title.studio_id = studio_id
    },
    toTitleStatus({commit, state}, status) {
      commit('setTitleStatus', status);
      state.title.status = status
    },
    toTitleTitleType({commit, state}, title_type) {
      commit('setTitleTitleType', title_type);
      state.title.title_type = title_type
    },
    toTitleTotalOfEpisode({commit, state}, total_of_episode) {
      commit('setTitleTotalOfEpisode', total_of_episode);
      state.title.total_of_episode = total_of_episode
    },
    toTitleRating({commit, state}, rating) {
      commit('setTitleRating', rating);
      state.title.rating = rating
    },
    toTitleCountry({commit, state}, country) {
      commit('setTitleCountry', country);
      state.title.country = country
    },
    toTitleReleaseYear({commit, state}, release_year) {
      commit('setTitleReleaseYear', release_year);
      state.title.release_year = release_year
    },
    toTitleGenre({commit, state}, genre) {
      commit('setTitleGenre', genre);
      state.title.genre = genre
    },
    toTitleDuration({commit, state}, duration) {
      commit('setTitleDuration', duration);
      state.title.duration = duration
    },
    toTitleTitleCost({commit, state}, title_cost_list) {
      commit('setTitleTitleCost', title_cost_list);
      state.title.title_cost_list = title_cost_list
    },
    toTitleRight({commit, state}, right_list) {
      commit('setTitleRight', right_list);
      state.title.right_list = right_list
    },
    toTitleEpisode({commit, state}, episode_list) {
      commit('setTitleEpisode', episode_list);
      state.title.episode_list = episode_list
    },
    toTitleAkaSynopsis({commit, state}, aka_synopsis_list) {
      commit('setTitleAkaSynopsis', aka_synopsis_list);
      state.title.aka_synopsis_list = aka_synopsis_list
    },
    toTitleCast({commit, state}, cast_list) {
      commit('setTitleCast', cast_list);
      state.title.cast_list = cast_list
    },
    toTitleThumbnail({commit, state}, thumbnail_list) {
      commit('setTitleThumbnail', thumbnail_list);
      state.title.thumbnail_list = thumbnail_list
    },
    toTitleCost({commit, state}, cost) {
      commit('setTitleCost', cost);
      state.title.cost = cost
    },
    toTitleLocalize({commit, state}, localize_list) {
      commit('setTitleLocalize', localize_list);
      state.title.localize_list = localize_list
    },
  },

  getters: {
    id: state => state.title.id,
    name: state => state.title.name,
    vendor_id: state => state.title.vendor_id,
    studio_id: state => state.title.studio_id,
    status: state => state.title.status,
    title_type: state => state.title.title_type,
    total_of_episode: state => state.title.total_of_episode,
    rating: state => state.title.rating,
    country: state => state.title.country,
    release_year: state => state.title.release_year,
    genre: state => state.title.genre,
    duration: state => state.title.duration,
    title_cost_list: state => state.title.title_cost_list,
    right_list: state => state.title.right_list,
    episode_list: state => state.title.episode_list,
    aka_synopsis_list: state => state.title.aka_synopsis_list,
    cast_list: state => state.title.cast_list,
    thumbnail_list: state => state.title.thumbnail_list,
    cost: state => state.title.cost,
    localize_list: state => state.title.localize_list,
  }

});
