import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex);
export const storeDeals = new Vuex.Store({
  state: {
    deals: {
      deal_no: 'No deal no.',
      ref_no: '',
      date: '',
      status: '',
      description: '',
      vendor: '',
      studio: '',
      rights_start: '',
      end: '',
      remark: '',
      attach: '',
      detail: [
        {
          title_id: '', name: 'test', title_type: '', total_episode: '', duration: '', type: 'Linear'
        },
        {
          title_id: '', name: 'test1', title_type: '', total_episode: '', duration: '', type: 'Linear'
        },
        {
          title_id: '', name: 'test2', title_type: '', total_episode: '', duration: '', type: 'TVod'
        }
      ],
      territory: [
        {
          name: 'TH',
          status: false
        },
        {
          name: 'PH',
          status: false
        },
      ]
    }
  },
  mutations: {
    setDealsDealNo(state, deal_no) {
      state.deals.deal_no = deal_no
    },
    setDealsRefNo(state, ref_no) {
      state.deals.ref_no = ref_no
    },
    setDealsDate(state, date) {
      state.deals.date = date
    },
    setDealsStatus(state, status) {
      state.deals.status = status
    },
    setDealsDescription(state, description) {
      state.deals.description = description
    },
    setDealsVendor(state, vendor) {
      state.deals.vendor = vendor
    },
    setDealsStudio(state, studio) {
      state.deals.studio = studio
    },
    setDealsRightsStart(state, rights_start) {
      state.deals.rights_start = rights_start
    },
    setDealsEnd(state, rights_start) {
      state.deals.rights_start = rights_start
    },
    setDealsRemark(state, remark) {
      state.deals.remark = remark
    },
    setDealsAttach(state, attach) {
      state.deals.attach = attach
    },
    setDealsDetail(state, detail) {
      state.deals.detail = detail
    },
    setDealsTerritory(state, territory) {
      state.deals.territory = territory
    },
  },
  actions: {
    toDealsDealNo({commit, state}, deal_no) {
      commit('setDealsDealNo', deal_no);
      state.deals.deal_no = deal_no
    },
    toDealsRefNo({commit, state}, ref_no) {
      commit('setDealsRefNo', ref_no);
      state.deals.ref_no = ref_no
    },
    toDealsDate({commit, state}, date) {
      commit('setDealsDate', date);
      state.deals.date = date
    },
    toDealsStatus({commit, state}, status) {
      commit('setDealsStatus', status);
      state.deals.status = status
    },
    toDealsDescription({commit, state}, description) {
      commit('setDealsDescription', description);
      state.deals.description = description
    },
    toDealsVendor({commit, state}, vendor) {
      commit('setDealsVendor', vendor);
      state.deals.vendor = vendor
    },
    toDealsStudio({commit, state}, studio) {
      commit('setDealsStudio', studio);
      state.deals.studio = studio
    },
    toDealsRightsStart({commit, state}, rights_start) {
      commit('setDealsRightsStart', rights_start);
      state.deals.rights_start = rights_start
    },
    toDealsEnd({commit, state}, end) {
      commit('setDealsEnd', end);
      state.deals.end = end
    },
    toDealsRemark({commit, state}, remark) {
      commit('setDealsRemark', remark);
      state.deals.remark = remark
    },
    toDealsAttach({commit, state}, attach) {
      commit('setDealsAttach', attach);
      state.deals.attach = attach
    },
    toDealsDetail({commit, state}, detail) {
      commit('setDealsDetail', detail);
      state.deals.detail = detail
    },
    toDealsTerritory({commit, state}, territory) {
      commit('setDealsTerritory', territory);
      state.deals.territory = territory
    },
  },
  getters: {
    deal_no: state => state.deals.deal_no,
    ref_no: state => state.deals.ref_no,
    date: state => state.deals.date,
    status: state => state.deals.status,
    description: state => state.deals.description,
    vendor: state => state.deals.vendor,
    studio: state => state.deals.studio,
    rights_start: state => state.deals.rights_start,
    end: state => state.deals.end,
    remark: state => state.deals.remark,
    attach: state => state.deals.attach,
    detail: state => state.deals.detail,
    territory: state => state.deals.territory
  }
});
