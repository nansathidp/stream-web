import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex);
export const storePublishTools = new Vuex.Store({
  state: {
    publish_tools: {
      start: '',
      end: '',
      publish_zone: [],
      episode: []
    }
  },

  mutations: {
    setPublishToolsStart(state, start) {
      state.publish_tools.start = start
    },
    setPublishToolsEnd(state, end) {
      state.publish_tools.end = end
    },
    setPublishToolsPublishZone(state, publish_zone) {
      state.publish_tools.publish_zone = publish_zone
    },
    setPublishToolsEpisodeChosen(state, episode) {
      state.publish_tools.episode = episode
    },
  },
  actions: {
    toPublishToolsStart({commit, state}, start) {
      commit('setPublishToolsStart', start);
      state.publish_tools.start = start
    },
    toPublishToolsEnd({commit, state}, end) {
      commit('setPublishToolsEnd', end);
      state.publish_tools.end = end
    },
    toPublishToolsPublishZone({commit, state}, publish_zone) {
      commit('setPublishToolsPublishZone', publish_zone);
      state.publish_tools.publish_zone = publish_zone
    },
    toPublishToolsEpisodeChosen({commit, state}, episode) {
      commit('setPublishToolsEpisodeChosen', episode);
      state.publish_tools.episode = episode
    },
  },
  getters: {
    start: state => state.publish_tools.start,
    end: state => state.publish_tools.end,
    publish_zone: state => state.publish_tools.publish_zone,
    episode: state => state.publish_tools.episode,
  }
});
