import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex);
export const storePublishZone = new Vuex.Store({
  state: {
    publish_zone: {
      publish_zone_id: '',
      zone_name: '',
      drm: '',
      country: [],
      sub_title_language: [],
      audio_language: [],
      interface_cms: [],
      bitrate_resolution: [],
      location: []
    }
  },

  mutations: {
    setPublishZonePublishZoneId(state, publish_zone_id) {
      state.publish_zone.publish_zone_id = publish_zone_id
    },
    setPublishZoneZoneName(state, zone_name) {
      state.publish_zone.zone_name = zone_name
    },
    setPublishZoneDrm(state, drm) {
      state.publish_zone.drm = drm
    },
    setPublishZoneCountry(state, country) {
      state.publish_zone.country = country
    },
    setPublishZoneSubTitleLanguage(state, sub_title_language) {
      state.publish_zone.sub_title_language = sub_title_language
    },
    setPublishZoneAudioLanguage(state, audio_language) {
      state.publish_zone.audio_language = audio_language
    },
    setPublishZoneInterfaceCms(state, interface_cms) {
      state.publish_zone.interface_cms = interface_cms
    },
    setPublishZoneBitrateResolution(state, bitrate_resolution) {
      state.publish_zone.bitrate_resolution = bitrate_resolution
    },
  },
  actions: {
    toPublishZonePublishZoneId({commit, state}, publish_zone_id) {
      commit('setPublishZonePublishZoneId', publish_zone_id);
      state.publish_zone.publish_zone_id = publish_zone_id
    },
    toPublishZoneZoneName({commit, state}, zone_name) {
      commit('setPublishZoneZoneName', zone_name);
      state.publish_zone.zone_name = zone_name
    },
    toPublishZoneDrm({commit, state}, drm) {
      commit('setPublishZoneDrm', drm);
      state.publish_zone.drm = drm
    },
    toPublishZoneCountry({commit, state}, country) {
      commit('setPublishZoneCountry', country);
      state.publish_zone.country = country
    },
    toPublishZoneSubTitleLanguage({commit, state}, sub_title_language) {
      commit('setPublishZoneSubTitleLanguage', sub_title_language);
      state.publish_zone.sub_title_language = sub_title_language
    },
    toPublishZoneAudioLanguage({commit, state}, audio_language) {
      commit('setPublishZoneAudioLanguage', audio_language);
      state.publish_zone.audio_language = audio_language
    },
    toPublishZoneInterfaceCms({commit, state}, interface_cms) {
      commit('setPublishZoneInterfaceCms', interface_cms);
      state.publish_zone.interface_cms = interface_cms
    },
    toPublishZoneBitrateResolution({commit, state}, bitrate_resolution) {
      commit('setPublishZoneBitrateResolution', bitrate_resolution);
      state.publish_zone.bitrate_resolution = bitrate_resolution
    },
    toPublishZoneLocation({commit, state}, location) {
      commit('setPublishZoneLocation', location);
      state.publish_zone.location = location
    },
  },
  getters: {
    publish_zone_id: state => state.publish_zone.publish_zone_id,
    publish_zone: state => state.publish_zone.publish_zone,
    drm: state => state.publish_zone.drm,
    country: state => state.publish_zone.country,
    sub_title_language: state => state.publish_zone.sub_title_language,
    audio_language: state => state.publish_zone.audio_language,
    interface_cms: state => state.publish_zone.interface_cms,
    bitrate_resolution: state => state.publish_zone.bitrate_resolution,
    location: state => state.publish_zone.location,
  }
});
