export default {
  items: [
    {
      title: true,
      name: 'Home',
      class: '',
      wrapper: {
        element: '',
        attributes: {},
      }
    },
    {
      name: 'Home',
      url: '/home',
      icon: 'icon-home',
    },
    {
      name: 'Dashboard',
      url: '/dashboard',
      icon: 'icon-home',
      children: [
        {
          name: 'Contract',
          url: '/dashboard/contract',
          icon: 'icon-doc',
          code: "deal.view_deal"
        },
      ]
    },
    {
      name: 'Program Library',
      url: '/program',
      icon: 'icon-folder',
      children: [
        {
          name: 'Deal',
          url: '/program/deal-list',
          icon: 'icon-doc',
          code: "deal.view_deal"
        },
        {
          name: 'Title',
          url: '/program/title-list',
          icon: 'icon-doc',
          code: "title.view_title"
        },
        {
          name: 'Episode',
          url: '/program/episode-list',
          icon: 'icon-doc',
          code: "episode.view_episode"
        },
        {
          name: 'Publish',
          url: '/program/publish',
          icon: 'icon-folder',
          code: 'episode.add_publishepisode',
          children: [
            {
              name: 'Publish Tools',
              url: '/program/publish-tools',
              icon: 'icon-doc',
              code: 'episode.add_publishepisode'
            },
            {
              name: 'Schedule Publish',
              url: '/program/schedule-publish',
              icon: 'icon-doc',
              code: 'episode.add_publishepisode'
            },
          ]
        },
      ]
    },
    {
      name: 'Asset',
      url: '/asset',
      icon: 'icon-folder',
      code: 'asset.view_asset',
      children: [
        {
        name: 'Asset',
        url: '/asset/asset',
        code: 'asset.view_detail_asset',
        icon: 'icon-docs',
      },
      {
        name: 'Asset Tracking',
        url: '/asset/asset-tracking',
        code: 'asset.view_detail_asset',
        icon: 'icon-docs',
      }
      ]
    },
    {
      name: 'AKA',
      url: '/aka',
      icon: 'icon-folder',
      children: [
        {
          name: 'AKA Title',
          url: '/aka/aka-title',
          icon: 'icon-doc',
          code: 'title.add_aka'
        },
        {
          name: 'AKA Episode',
          url: '/aka/aka-episode',
          icon: 'icon-doc',
          code: 'episode.add_akaepisode'
        }
      ]
    },
    {
      name: 'Live Channel',
      url: '/live-channel',
      icon: 'icon-folder',
      code: 'channel.view_channel',
      children: [
        {
          name: 'EPG',
          url: '/live-channel/epg',
          icon: 'icon-doc',
          code: 'epg.view_schedule'
        },
        {
          name: 'Channel',
          url: '/live-channel/channel',
          icon: 'icon-doc',
          code: 'channel.view_channel'
        }
      ]
    },
    {
      name: 'Censor',
      url: '/censor',
      icon: 'icon-folder',
      code: 'censor.view_censor',
      children: [{
        name: 'Censor',
        url: '/censor/censor-list',
        code: 'censor.view_detail_censor',
        icon: 'icon-docs',
      },]
    },
    {
      name: 'System Management',
      url: '/system-management',
      icon: 'icon-folder',
      children: [
        {
          name: 'Amortize',
          url: '/master-data/amortize',
          icon: 'icon-doc',
          code: 'amortize.view_category'
        },
        {
          name: 'Authorized',
          url: '/authorized/add-user',
          icon: 'icon-doc',
          code: 'account.add_account'
        },
        {
          name: 'Cost',
          url: '/master-data/cost',
          icon: 'icon-doc',
          code: 'cost.view_cost'
        },
        {
          name: 'Country',
          url: '/master-data/country',
          icon: 'icon-doc',
          code: 'country.view_country'
        },
        {
          name: 'Genre',
          url: '/master-data/genre',
          icon: 'icon-doc',
          code: 'genre.view_genre'
        },
        {
          name: 'Language',
          url: '/master-data/language',
          icon: 'icon-doc',
          code: 'language.view_language'
        },
        {
          name: 'OtherType',
          url: '/master-data/other-type',
          icon: 'icon-doc',
          code: 'other_type.view_type'
        },
        {
          name: 'Owner',
          url: '/master-data/owner',
          icon: 'icon-doc',
          code: 'owner.view_businessowner'
        },
        {
          name: 'People',
          url: '/master-data/people',
          icon: 'icon-doc',
          code: 'people.view_people'
        },
        {
          name: 'Publish Zone',
          url: '/master-data/publish-zone',
          icon: 'icon-doc',
          code: 'publish.view_publish'
        },
        {
          name: 'Rating',
          url: '/master-data/rating',
          icon: 'icon-doc',
          code: 'rating.view_rating'
        },
        {
          name: 'Reason',
          url: '/master-data/reason',
          icon: 'icon-doc',
          code: 'reason.view_censor'
        },
        {
          name: 'Role',
          url: '/master-data/role',
          icon: 'icon-doc',
          code: 'role.view_role'
        },
        {
          name: 'Studio',
          url: '/master-data/studio',
          icon: 'icon-doc',
          code: 'studio.view_studio'
        },
        {
          name: 'Sub Genre',
          url: '/master-data/sub-genre',
          icon: 'icon-doc',
          code: 'sub_genre.view_sub_genre'
        },
        {
          name: 'Title Group',
          url: '/system-management/title-group',
          icon: 'icon-folder',
          code: 'title_group.view_group',
          children: [
            {
              name: 'Group Name',
              url: '/system-management/title-group/group-name',
              icon: 'icon-doc',
              code: "title_group.view_detail_group"
            },
            {
              name: 'Structure',
              url: '/system-management/title-group/structure',
              icon: 'icon-doc',
              code: "title_group.view_structure"
            },
          ]
        },
        {
          name: 'Vendor',
          url: '/master-data/vendor',
          icon: 'icon-doc',
          code: 'vendor.view_vendor'
        },
      ]
    },
  ]
}
