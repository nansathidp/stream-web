import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    userUserPermission: [],
  },
  getters: {
    getPermission: state => app => state.userUserPermission.filter(codename => codename.includes(app))
  },
  actions: {
    initPermission: ({commit, state}, userPermission) => {
      commit('setPermission', userPermission)
    },
    destroyPermission: ({commit, state}) => {
      commit('setPermission', [])
    }

  },
  mutations: {
    setPermission: (state, userPermission) => {
      state.userUserPermission = userPermission
    },

  }

})
