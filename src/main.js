// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.scrolling.conf with an alias.
import 'core-js/es6/promise'
import 'core-js/es6/string'
import 'core-js/es7/array'
// import cssVars from 'css-vars-ponyfill'
import Vue from 'vue'
import './plugins/service'
import BootstrapVue from 'bootstrap-vue'
import App from './App'
import router from './routers'
import store from './vuex/store'
import Antd from 'ant-design-vue'
import 'ant-design-vue/dist/antd.css'
import Datepicker from 'vuejs-datepicker';
import VCalendar from 'v-calendar';
import '@progress/kendo-ui'
import { DateinputsInstaller, Calendar, DateInput, DatePicker, DateRangePicker, DateTimePicker, TimePicker, MultiViewCalendar } from '@progress/kendo-dateinputs-vue-wrapper';
import '@progress/kendo-theme-bootstrap/dist/all.css'
import VueTimepicker from 'vue2-timepicker'
import 'vue2-timepicker/dist/VueTimepicker.css'
import { Scheduler } from '@progress/kendo-scheduler-vue-wrapper'
import { SchedulerResource } from '@progress/kendo-scheduler-vue-wrapper'
import { SchedulerView } from '@progress/kendo-scheduler-vue-wrapper'
import { SchedulerInstaller } from '@progress/kendo-scheduler-vue-wrapper'
import ElementUI from 'element-ui'
import locale from 'element-ui/lib/locale/lang/en'
import 'element-ui/lib/theme-chalk/index.css'
// todo
// cssVars()
Vue.use(ElementUI, { locale })
Vue.use(DateinputsInstaller)
Vue.use(VCalendar)
Vue.use(Antd)
Vue.use(BootstrapVue);
Vue.use(SchedulerInstaller)
Vue.prototype.$super = function(options) {
    return new Proxy(options, {
        get: (options, name) => {
            if (options.methods && name in options.methods) {
                return options.methods[name].bind(this);
            }
        },
    });
};
/* eslint-disable no-new */
new Vue({
    el: '#app',
    router,
    template: '<App/>',
    store,

    components: {
        App,
        Datepicker,
        VueTimepicker,
        Scheduler
    }
})
