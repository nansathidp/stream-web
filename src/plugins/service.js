import Vue from 'vue';
import Service from '../services/index';

Plugin.install = function (Vue, options) {
  Vue.services = Service;
  window.services = Service;
  Object.defineProperties(Vue.prototype, {
    services: {
      get() {
        return Service;
      }
    },
    $services: {
      get() {
        return Service;
      }
    },
  });
};

Vue.use(Plugin);
export default Plugin;
