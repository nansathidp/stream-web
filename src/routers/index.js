import Vue from 'vue'
import Router from 'vue-router'



// Containers
const DefaultContainer = () => import('@/containers/DefaultContainer')


// Views

const AuthorizedUser = () => import('@/views/authorized/AuthorizedUser')
const Account = () => import('@/views/authorized/account')

const Colors = () => import('@/views/theme/Colors')
const Typography = () => import('@/views/theme/Typography')

const Charts = () => import('@/views/Charts')
const Widgets = () => import('@/views/Widgets')

// Views - Home
const Home = () => import('@/views/Home')

// Views - Dashboard
const Contract = () => import('@/views/dashboard/Contract')

// Views - Components
// const Cards = () => import('@/views/scrolling/Cards')
const Forms = () => import('@/views/scrolling/Forms')
const Switches = () => import('@/views/scrolling/Switches')
const Tabs = () => import('@/views/scrolling/Tabs')
const Breadcrumbs = () => import('@/views/scrolling/Breadcrumbs')
const Carousels = () => import('@/views/scrolling/Carousels')
const Collapses = () => import('@/views/scrolling/Collapses')
const Jumbotrons = () => import('@/views/scrolling/Jumbotrons')

const Navs = () => import('@/views/scrolling/Navs')
const Navbars = () => import('@/views/scrolling/Navbars')
const Paginations = () => import('@/views/scrolling/Paginations')
const Popovers = () => import('@/views/scrolling/Popovers')
const ProgressBars = () => import('@/views/scrolling/ProgressBars')
const Tooltips = () => import('@/views/scrolling/Tooltips')

// View - Scrolling Text
const ScrollingText = () => import('@/views/scrolling/ScrollingText')
const ScrollingConfig = () => import('@/views/scrolling/ScrollingConfig')

// View - Program
const Deal = () => import('@/views/program/Deal')
const DealsList = () => import('@/views/program/DealList')
const TitleList = () => import('@/views/program/TitleList')
const Title = () => import('@/views/program/Title')
const EpisodeList = () => import('@/views/program/EpisodeList')
const Episode = () => import('@/views/program/Episode')
const PublishTools = () => import('@/views/program/publish/PublishTools')
const SchedulePublish = () => import('@/views/program/publish/SchedulePublish')
const GroupName = () => import('@/views/system-management/title-group/GroupName')
const Structure = () => import('@/views/system-management/title-group/Structure')
const Content = () => import('@/views/program/Content')
const Series = () => import('@/views/program/Series')
const GenerateContent = () => import('@/views/program/GenerateContent')
const GenerateSeries = () => import('@/views/program/GenerateSeries')
const Meta = () => import('@/views/program/Meta')
const GenerateMeta = () => import('@/views/program/GenerateMeta')
const License = () => import('@/views/program/ManageLicense')

// View - Aka
const AKATitle = () => import('@/views/aka/AkaTitle')
const AKAEpisode = () => import('@/views/aka/AkaEpisode')

// View - Master Data
const Rating = () => import('@/views/master-data/Rating')
const People = () => import('@/views/master-data/People')
const Role = () => import('@/views/master-data/Role')
const Studio = () => import('@/views/master-data/Studio')
const Vendor = () => import('@/views/master-data/Vendor')
const Language = () => import('@/views/master-data/Language')
const Country = () => import('@/views/master-data/Country')
const Genre = () => import('@/views/master-data/Genre')
const SubGenre = () => import('@/views/master-data/SubGenre')
const PublishZone = () => import('@/views/master-data/PublishZone')
const PublishZoneCreate = () => import('@/views/master-data/PublishZoneCreate')
const OtherType = () => import('@/views/master-data/OtherType')
const Owner = () => import('@/views/master-data/Owner')
const Cost = () => import('@/views/master-data/Cost')
const Amortize = () => import('@/views/master-data/Amortize')
const Reason = () => import('@/views/master-data/Reason')

// View - Live Channel
const Channel = () => import('@/views/live-channel/Channel')
const EPG = () => import('@/views/live-channel/EPG')
const LiveRecord = () => import('@/views/live-channel/LiveRecord')
const Blackout = () => import('@/views/live-channel/Blackout')
// const ASSET = () => import('@/views/live-channel/Asset')
// const DetailAsset = () => import('@/views/live-channel/DetailAsset')

// View - L2F
// const L2F = () => import('@/views/L2f')

//View - Scheduler
const Scheduler = () => import('@/views/schedule/Scheduler')

//View - Asset
// const Asset = () => import('@/views/asset/Asset')
// const AssetInfo = () => import('@/views/asset/AssetInfo')
// const DetailAssetInfo = () => import('@/views/asset/DetailAssetInfo')
// const AssetCategory = () => import('@/views/asset/AssetCategory')
// const AssetNode = () => import('@/views/asset/AssetNode')
// const DetailAssetNode = () => import('@/views/asset/DetailAssetNode')
// const DetailAssetCategory = () => import('@/views/asset/DetailAssetCategory')

//View - Asset
const Asset = () => import('@/views/assetv2/Asset')
const AssetTracking = () => import('@/views/assetv2/AssetTracking')
const AssetPreview = () => import('@/views/assetv2/AssetPreview')

//View - Censor
const CensorList = () => import('@/views/censor/CensorList')
const Censor = () => import('@/views/censor/Censor')

// Views - Buttons
const StandardButtons = () => import('@/views/buttons/StandardButtons')
const ButtonGroups = () => import('@/views/buttons/ButtonGroups')
const Dropdowns = () => import('@/views/buttons/Dropdowns')
const BrandButtons = () => import('@/views/buttons/BrandButtons')

// Views - Icons
const Flags = () => import('@/views/icons/Flags')
const FontAwesome = () => import('@/views/icons/FontAwesome')
const SimpleLineIcons = () => import('@/views/icons/SimpleLineIcons')
const CoreUIIcons = () => import('@/views/icons/CoreUIIcons')

// Views - Notifications
const Alerts = () => import('@/views/notifications/Alerts')
const Badges = () => import('@/views/notifications/Badges')
const Modals = () => import('@/views/notifications/Modals')

// Views - Pages
const Page401 = () => import('@/views/pages/Page401')
const Page403 = () => import('@/views/pages/Page403')
const Page404 = () => import('@/views/pages/Page404')
const Page500 = () => import('@/views/pages/Page500')
const Page502 = () => import('@/views/pages/Page502')
const Login = () => import('@/views/pages/Login')
const LoadLogin = () => import('@/views/pages/LoadLogin')
const Register = () => import('@/views/pages/Register')
const ContactAdmin = () => import('@/views/pages/ContactAdmin')

// Users
const Users = () => import('@/views/users/Users')
const User = () => import('@/views/users/User')

Vue.use(Router)

export default new Router({
  mode: 'history', // https://router.vuejs.org/api/#mode
  linkActiveClass: 'open active',
  scrollBehavior: () => ({y: 0}),
  routes: [
    {
      path: '/loads',
      redirect: '/loads/login',
      name: 'LoadLogin',
      component: {
        render(c) {
          return c('router-view')
        }
      },
      children: [
        {
          path: 'login',
          name: 'Load-login',
          component: LoadLogin,
        }
      ],
    },
    {
      path: '',
      redirect: '/pages/login',
      name: 'Login',
      component: Login
    },
    {
      path: '',
      redirect: '',
      name: '',
      component: DefaultContainer,
      children: [
        {
          path: 'home',
          redirect: '',
          name: 'Home',
          component: Home
        },
        {
          path: 'theme',
          redirect: '/theme/colors',
          name: 'Theme',
          component: {
            render(c) {
              return c('router-view')
            }
          },
          children: [
            {
              path: 'colors',
              name: 'Colors',
              component: Colors
            },
            {
              path: 'typography',
              name: 'Typography',
              component: Typography
            }
          ]
        },
        {
          path: 'charts',
          name: 'Charts',
          component: Charts
        },
        {
          path: 'widgets',
          name: 'Widgets',
          component: Widgets
        },
        {
          path: 'users',
          meta: {label: 'Users'},
          component: {
            render(c) {
              return c('router-view')
            }
          },
          children: [
            {
              path: '',
              component: Users,
            },
            {
              path: ':id',
              meta: {label: 'User Details'},
              name: 'User',
              component: User,
            },
          ]
        },
        {
          path: 'scrolling',
          redirect: '/scrolling/cards',
          name: 'Scrolling',
          component: {
            render(c) {
              return c('router-view')
            }
          },
          children: [
            {
              path: 'scrolling-text',
              redirect: '',
              name: 'Scrolling Text',
              component: ScrollingText
            },
            {
              path: 'scrolling-config',
              name: 'Scrolling Config',
              component: ScrollingConfig
            },
            {
              path: 'forms',
              name: 'Forms',
              component: Forms
            },
            {
              path: 'switches',
              name: 'Switches',
              component: Switches
            },
            {
              path: 'tabs',
              name: 'Tabs',
              component: Tabs
            },
            {
              path: 'breadcrumbs',
              name: 'Breadcrumbs',
              component: Breadcrumbs
            },
            {
              path: 'carousels',
              name: 'Carousels',
              component: Carousels
            },
            {
              path: 'collapses',
              name: 'Collapses',
              component: Collapses
            },
            {
              path: 'jumbotrons',
              name: 'Jumbotrons',
              component: Jumbotrons
            },
            {
              path: 'navs',
              name: 'Navs',
              component: Navs
            },
            {
              path: 'navbars',
              name: 'Navbars',
              component: Navbars
            },
            {
              path: 'paginations',
              name: 'Paginations',
              component: Paginations
            },
            {
              path: 'popovers',
              name: 'Popovers',
              component: Popovers
            },
            {
              path: 'progress-bars',
              name: 'Progress Bars',
              component: ProgressBars
            },
            {
              path: 'tooltips',
              name: 'Tooltips',
              component: Tooltips
            }
          ]
        },
        {
          path: 'program',
          redirect: '',
          name: 'Program',
          component: {
            render(c) {
              return c('router-view')
            }
          },
          children: [
            {
              path: 'deal-list',
              name: 'Deal List',
              component: DealsList
            },
            {
              path: 'deal/:id',
              name: 'Deal',
              component: Deal
            },
            {
              path: 'deal',
              name: 'Deal',
              component: Deal
            },
            {
              path: 'title-list',
              name: 'Title List',
              component: TitleList
            },
            {
              path: 'title/:id',
              name: 'Title',
              component: Title
            },
            {
              path: 'title',
              name: 'Title',
              component: Title
            },
            {
              path: 'episode-list',
              name: 'Episode List',
              component: EpisodeList
            },
            {
              path: 'episode/:id',
              name: 'Episode',
              component: Episode
            },
            {
              path: 'episode',
              name: 'Episode',
              component: Episode
            },
            {
              path: 'publish-tools',
              name: 'PublishTools',
              component: PublishTools
            },
            {
              path: 'schedule-publish',
              name: 'SchedulePublish',
              component: SchedulePublish
            },
            {
              path: 'content',
              name: 'Content',
              component: Content
            },
            {
              path: 'series',
              name: 'Series',
              component: Series
            },
            {
              path: 'generate-content/:id',
              name: 'GenerateContent',
              component: GenerateContent
            },
            {
              path: 'generate-content',
              name: 'GenerateContent',
              component: GenerateContent
            },
            {
              path: 'generate-series/:id',
              name: 'GenerateSeries',
              component: GenerateSeries
            },
            {
              path: 'generate-series',
              name: 'GenerateSeries',
              component: GenerateSeries
            },
            {
              path: 'manage-license',
              name: 'ManageLicense',
              component: License
            },
            {
              path: 'meta',
              name: 'Meta',
              component: Meta
            },
            {
              path: 'generate-meta',
              name: 'GenerateMeta',
              component: GenerateMeta
            },
            {
              path: 'generate-meta/:id',
              name: 'GenerateMeta',
              component: GenerateMeta
            }
          ]
        },
        {
          path: 'dashboard',
          redirect: '',
          name: 'Dashboard',
          component: {
            render(c) {
              return c('router-view')
            }
          },
          children: [
            {
              path: 'contract',
              redirect: '',
              name: 'Contract',
              component: Contract
            },
          ]
        },

        {
          path: 'system-management',
          redirect: '',
          name: 'System Management',
          component: {
            render(c) {
              return c('router-view')
            }
          },
          children: [
            {
              path: 'title-group',
              redirect: '',
              name: 'Title Group',
              component: {
                render(c) {
                  return c('router-view')
                }
              },
              children: [
                {
                  path: 'group-name',
                  name: 'GroupName',
                  component: GroupName
                },
                {
                  path: 'structure',
                  name: 'Structure',
                  component: Structure
                },
              ]
            }
          ]
        },
        {
          path: 'aka',
          redirect: '',
          name: 'AKA',
          component: {
            render(c) {
              return c('router-view')
            }
          },
          children: [
            {
              path: 'aka-title',
              name: 'AKA Title',
              component: AKATitle
            },
            {
              path: 'aka-Episode',
              name: 'AKA Episode',
              component: AKAEpisode
            },
          ]
        },
        {
          path: 'master-data',
          redirect: '',
          name: 'MasterData',
          component: {
            render(c) {
              return c('router-view')
            }
          },
          children: [
            {
              path: 'people',
              name: 'People',
              component: People
            },
            {
              path: 'role',
              name: 'Role',
              component: Role
            },
            {
              path: 'rating',
              name: 'Rating',
              component: Rating
            },
            {
              path: 'studio',
              name: 'Studio',
              component: Studio
            },
            {
              path: 'vendor',
              name: 'Vendor',
              component: Vendor
            },
            {
              path: 'language',
              name: 'language',
              component: Language
            },
            {
              path: 'country',
              name: 'Country',
              component: Country
            },
            {
              path: 'genre',
              name: 'Genre',
              component: Genre
            },
            {
              path: 'sub-genre',
              name: 'SubGenre',
              component: SubGenre
            },
            {
              path: 'other-type',
              name: 'OtherType',
              component: OtherType
            },
            {
              path: 'publish-zone',
              name: 'PublishZone',
              component: PublishZone
            },
            {
              path: 'publish-zone-create',
              name: 'PublishZoneCreate',
              component: PublishZoneCreate
            },
            {
              path: 'publish-zone-create/:id',
              name: 'PublishZoneCreate',
              component: PublishZoneCreate,
              props: (route) => ({query: route.query.q})
            },
            {
              path: 'owner',
              name: 'Owner',
              component: Owner
            },
            {
              path: 'cost',
              name: 'Cost',
              component: Cost
            },
            {
              path: 'amortize',
              name: 'Amortize',
              component: Amortize
            },
            {
              path: 'reason',
              name: 'Reason',
              component: Reason
            },
          ]
        },
        {
          path: 'schedule',
          redirect: '',
          name: 'Schedule',
          component: {
            render(c) {
              return c('router-view')
            }
          },
          children: [
            {
              path: 'scheduler',
              name: 'Scheduler',
              component: Scheduler
            }
          ]
        },
        {
          path: 'live-channel',
          redirect: '',
          name: 'Live channel',
          component: {
            render(c) {
              return c('router-view')
            }
          },
          children: [
            {
              path: 'channel',
              redirect: '',
              name: 'Channel',
              component: Channel
            },
            {
              path: 'epg',
              redirect: '',
              name: 'Epg',
              component: EPG
            },
            {
              path: 'live-record',
              redirect: '',
              name: 'LiveRecord',
              component: LiveRecord
            },
            {
              path: 'blackout',
              redirect: '',
              name: 'Blackout',
              component: Blackout
            },
          ]
        },
        {
          path: 'authorized',
          redirect: '',
          name: 'Authorized',
          component: {
            render(c) {
              return c('router-view')
            }
          },
          children: [
            {
              path: 'add-user',
              name: 'Add User',
              component: Account
            },
            {
              path: 'authorized-user',
              name: 'Authorized User',
              component: AuthorizedUser
            },
          ]
        },
        {
          path: 'censor',
          redirect: '',
          name: 'Censor',
          component: {
            render(c) {
              return c('router-view')
            }
          },
          children: [
            {
              path: 'censor-list',
              name: 'Censor List',
              component: CensorList
            },
            {
              path: 'censor/:id',
              name: 'Censor',
              component: Censor
            },
            {
              path: 'censor',
              name: 'Censor',
              component: Censor
            },
          ]
        },
        {
          path: 'asset',
          redirect: '',
          name: 'Asset',
          component: {
            render(c) {
              return c('router-view')
            }
          },
          children: [
            {
              path: 'asset-preview/:id',
              name: 'Asset Preview',
              component: AssetPreview
            },
            {
              path: 'asset-preview',
              name: 'Asset Preview',
              component: AssetPreview
            },
            {
              path: 'asset',
              name: 'Asset',
              component: Asset
            },
            {
              path: 'asset-tracking',
              name: 'Asset Tracking',
              component: AssetTracking
            },
          ]
        },
        {
          path: 'buttons',
          redirect: '/buttons/standard-buttons',
          name: 'Buttons',
          component: {
            render(c) {
              return c('router-view')
            }
          },
          children: [
            {
              path: 'standard-buttons',
              name: 'Standard Buttons',
              component: StandardButtons
            },
            {
              path: 'button-groups',
              name: 'Button Groups',
              component: ButtonGroups
            },
            {
              path: 'dropdowns',
              name: 'Dropdowns',
              component: Dropdowns
            },
            {
              path: 'brand-buttons',
              name: 'Brand Buttons',
              component: BrandButtons
            }
          ]
        },
        {
          path: 'icons',
          redirect: '/icons/font-awesome',
          name: 'Icons',
          component: {
            render(c) {
              return c('router-view')
            }
          },
          children: [
            {
              path: 'coreui-icons',
              name: 'CoreUI Icons',
              component: CoreUIIcons
            },
            {
              path: 'flags',
              name: 'Flags',
              component: Flags
            },
            {
              path: 'font-awesome',
              name: 'Font Awesome',
              component: FontAwesome
            },
            {
              path: 'simple-line-icons',
              name: 'Simple Line Icons',
              component: SimpleLineIcons
            }
          ]
        },
        {
          path: 'notifications',
          redirect: '/notifications/alerts',
          name: 'Notifications',
          component: {
            render(c) {
              return c('router-view')
            }
          },
          children: [
            {
              path: 'alerts',
              name: 'Alerts',
              component: Alerts
            },
            {
              path: 'badges',
              name: 'Badges',
              component: Badges
            },
            {
              path: 'modals',
              name: 'Modals',
              component: Modals
            }
          ]
        }
      ]
    },
    {
      path: '/pages',
      redirect: '/pages/404',
      name: 'Pages',
      component: {
        render(c) {
          return c('router-view')
        }
      },
      children: [
        {
          path: '401',
          name: 'Page401',
          component: Page401
        },
        {
          path: '403',
          name: 'Page403',
          component: Page403
        },
        {
          path: '404',
          name: 'Page404',
          component: Page404
        },
        {
          path: '500',
          name: 'Page500',
          component: Page500
        },
        {
          path: 'login',
          name: 'Login',
          component: Login
        },
        {
          path: 'load-login',
          name: 'LoadLogin',
          component: LoadLogin
        },
        {
          path: '502',
          name: 'Page502',
          component: Page502
        },
        {
          path: 'register',
          name: 'Register',
          component: Register
        },
        {
          path: 'contact-admin',
          name: 'ContactAdmin',
          component: ContactAdmin
        }
      ]
    },
    {path: "*", component: Page404}
  ]
})
