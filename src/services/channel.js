import {Base} from "./shares";

export default class extends Base {
  async fetch() {
    return await this.get(`channel/`).then(this.response);
  }

  async find(params = {}) {
    return await this.get(`management/channel/`, params).then(this.response);
  }

  async addChannel(channel) {
    return await this.post(`management/channel/`, channel).then(this.response);
  }

  async findById(code) {
    return await this.get(`management/channel/${code}/`).then(this.response);
  }

  async updateChannel(code, channel) {
    return await this.put(`management/channel/${code}/`, channel).then(this.response);
  }

  async deleteChannel(code) {
    return await this.delete(`management/channel/${code}/`).then(this.response);
  }

}
