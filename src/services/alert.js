import { Base } from "./shares";

export default class AlertService extends Base {
    findById(id) {
        return this.get(`/alert/${id}/`).then(({ data }) => data)
    }
    findByIdDownload(id) {
      return this.get(`/alert/${id}/download/`)
    }
}
