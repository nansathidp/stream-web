import {Base} from "./shares";


export default class extends Base {
  async find(params = {}) {
    return await this.get(`epg/`, params).then(this.response);
  }

  async searchEpg(startDate, endDate, search) {
    return await this.get(`epg/${startDate}/${endDate}/?search=` + search);
  }
}
