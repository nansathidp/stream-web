import {Base} from "./shares";

export default class extends Base {
  find(params = {}) {
    return this.get(`rating/`, params).then(this.response);
  }

  read(params = {}) {
  	return this.get(`management/rating/`, params).then(this.response)
  }

  add(form) {
  	return this.post(`management/rating/`, form).then(this.response)
  }

  update(form, id) {
  	return this.put(`management/rating/${id}/`, form).then(this.response)
  }

  remove(id) {
  	return this.delete(`management/rating/${id}/`).then(this.response)
  }
}
