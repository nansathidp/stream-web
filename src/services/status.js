import {Base} from "./shares";

export default class extends Base {
  async getAllStatus() {
    return await this.get(`status/`);
  }
}
