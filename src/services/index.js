import MessageService from './messages'
import ProgramService from './program'
import ChannelService from './channel'
import EpgService from './epg'
import AccountService from './account'
import ConfigService from './config'
import AssetService from './asset'
import ProfileService from './profile'
import TitleService from './title'
import EpisodeService from './episode'
import VendorService from './vendor'
import StudioService from './studio'
import StatusService from './status'
import TitleConfigService from './titleConfig'
import RatingService from './rating'
import CountryService from './country'
import GenreService from './genre'
import PeopleService from './people'
import RoleService from './role'
import CostService from './cost'
import LanguageService from "./language";
import PublishService from "./publish";
import DealService from "./deal";
import DealConfigService from "./dealConfig";
import AlertService from './alert';
import TitleGroupService from './titleGroup';
import OwnerService from './owner';
import AkaService from './aka';
import OtherTypeService from './otherType';
import SubGenreService from './subGenre';
import AmortizeService from './amortize';
import CensorService from './censor';
import ReasonService from './reason';
import DashboardService from './dashboard';

export default {
    message: new MessageService(),
    program: new ProgramService(),
    alert: new AlertService(),
    channel: new ChannelService(),
    epg: new EpgService(),
    cost: new CostService(),
    account: new AccountService(),
    language: new LanguageService(),
    scrollConfig: new ConfigService(),
    asset: new AssetService(),
    profile: new ProfileService(),
    title: new TitleService(),
    episode: new EpisodeService(),
    vendor: new VendorService(),
    studio: new StudioService(),
    status: new StatusService(),
    config: new TitleConfigService(),
    rating: new RatingService(),
    country: new CountryService(),
    genre: new GenreService(),
    people: new PeopleService(),
    role: new RoleService(),
    publish: new PublishService(),
    deal: new DealService(),
    dealConfig: new DealConfigService(),
    titleGroup: new TitleGroupService(),
    owner: new OwnerService(),
    aka: new AkaService(),
    otherType: new OtherTypeService(),
    subGenre: new SubGenreService(),
    amortize: new AmortizeService(),
    censor: new CensorService(),
    reason: new ReasonService(),
    dashboard: new DashboardService(),
}
