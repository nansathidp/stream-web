import {Base} from "./shares";

export default class extends Base{
  async find() {
    return await this.get(`/amortize/category/`).then(this.response)
  }

  read(params = {}) {
  	return this.get(`management/amortize/`, params).then(this.response)
  }

  add(form) {
  	return this.post(`management/amortize/`, form).then(this.response)
  }

  update(form, id) {
  	return this.put(`management/amortize/${id}/`, form).then(this.response)
  }

  remove(id) {
  	return this.delete(`management/amortize/${id}/`).then(this.response)
  }
}
