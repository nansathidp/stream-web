import axios from "axios";

const siteConfig = (backendApi, webUri) => ({backendApi, webUri})

const backEndConfig = function () {
  switch (process.env.NODE_ENV) {
    case "development":
      return siteConfig('https://rms-dev-api.stm.trueid.net/api/', 'https://rms-dev.stm.trueid.net')
    case "staging":
      return siteConfig('https://rms-stg-api.stm.trueid.net/api/', 'https://rms-stg.stm.trueid.net')
    case "production":
      return siteConfig('https://rmsapi.stm.trueid.net/api/', 'https://rms.stm.trueid.net')
    default:
      return siteConfig('http://localhost:8000/api/', 'https://localhost:8080')
  }
}

export class Base {
  constructor() {
    this.page = -1;
    const {backendApi, webUri} = backEndConfig()
    let config = {
      baseURL: backendApi,
      timeout: 60 * 1000, // Timeout
      xsrfCookieName: 'XSRF-TOKEN', // default
      xsrfHeaderName: 'X-XSRF-TOKEN', // default
      'Access-Control-Allow-Origin': webUri,
      withCredentials: true, // Check cross-site Access-Control,
    };
    this.http = axios.create(config);
    this.setToken();
  }

  setToken() {
    const TOKEN = localStorage.getItem('token');
    if (TOKEN) {
      this.http.defaults.headers.common['Authorization'] = TOKEN;
    } else {
      delete this.http.defaults.headers.common['Authorization']
    }

  }

  response({data}) {
    return data
  }

  responseResultOnPage({data}) {
    let {page, results} = data;
    this.page = page;
    return results
  }

  delete(url) {
    // this.setToken()
    return this.http.delete(url)
  }

  get(url, params = {}) {
    // this.setToken();
    return this.http.get(url, {params})
  }

  post(url, data) {
    // this.setToken()
    return this.http.post(url, data)
  }

  put(url, data) {
    // this.setToken()
    return this.http.put(url, data)
  }

  patch(url, data) {
    // this.setToken()
    return this.http.patch(url, data)
  }
}
