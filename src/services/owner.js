import {Base} from "./shares";

export default class extends Base{
  find(params) {
    return this.get('/owner/', params).then(this.response)
  }

  read(params = {}) {
  	return this.get(`management/owner/`, params).then(this.response)
  }

  add(form) {
  	return this.post(`management/owner/`, form).then(this.response)
  }

  update(form, id) {
  	return this.put(`management/owner/${id}/`, form).then(this.response)
  }

  remove(id) {
  	return this.delete(`management/owner/${id}/`).then(this.response)
  }
}
