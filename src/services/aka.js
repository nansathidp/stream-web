import {Base} from "./shares";

export default class extends Base {
  async akaTitleExport(file) {
    return await this.post(`/title/download_aka/`, file);
  }
  async akaTitleImport(file) {
    return await this.post(`/title/import_aka/`, file);
  }
  async akaEpisodeExport(file) {
    return await this.post(`/episode/download_aka/`, file);
  }
  async akaEpisodeImport(file) {
    return await this.post(`/episode/import_aka/`, file);
  }
}
