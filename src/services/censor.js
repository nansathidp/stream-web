import {Base} from "./shares";

export default class extends Base {
  find(params = {}) {
    return this.get(`censor/`, params).then(this.response)
  }

  async findById(episode) {
    return await this.get(`censor/${episode}/`).then(this.response);
  }

  async add(censor) {
    return await this.post(`censor/`, censor).then(this.response)
}
}