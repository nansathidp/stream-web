import {Base} from "./shares";

export default class extends Base {
  findGroup(params = {}) {
    return this.get(`management/title-group/group/`, params).then(this.response);
  }
  findGroupById(id) {
    return this.get(`management/title-group/group/${id}/`).then(this.response);
  }
  addGroupName(group_name) {
    return this.post(`management/title-group/group/`, group_name).then(this.response);
  }
  updateGroupNameById(id, body) {
    return this.put(`management/title-group/group/${id}/`, body).then(this.response)
  }
  removeGroupName(id) {
    // return this.delete(`title/${id}/`,).then(this.response);
    return this.delete(`management/title-group/group/${id}/`,).then(this.response);
  }
  findStructure(params = {}) {
    return this.get(`management/title-group/structure/`, params).then(this.response);
  }
  addStructure(structure) {
    return this.post(`management/title-group/structure/`, structure).then(this.response);
  }
  updateStructureById(id, body) {
    return this.put(`management/title-group/structure/${id}/`, body).then(this.response)
  }
  removeStructure(id) {
    // return this.delete(`title/${id}/`,).then(this.response);
    return this.delete(`management/title-group/structure/${id}/`,).then(this.response);
  }
  findStructureById(id) {
    return this.get(`management/title-group/structure/${id}/`).then(this.response);
  }
}
