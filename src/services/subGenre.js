import {Base} from "./shares";

export default class extends Base {
  find() {
    return this.get(`sub-genre/`).then(this.response);
  }

  read(params = {}) {
  	return this.get(`management/sub-genre/`, params).then(this.response)
  }

  add(form) {
  	return this.post(`management/sub-genre/`, form).then(this.response)
  }

	update(form, id) {
  	return this.put(`management/sub-genre/${id}/`, form).then(this.response)
  }

  remove(id) {
  	return this.delete(`management/sub-genre/${id}/`).then(this.response)
  }
}
