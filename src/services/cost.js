import {Base} from './shares'


export default class extends Base {
  find(params = {}) {
    return this.get('cost/', params).then(this.response)
  }

  read(params = {}) {
  	return this.get(`management/cost/`, params).then(this.response)
  }

  add(form) {
  	return this.post(`management/cost/`, form).then(this.response)
  }

  update(form, id) {
  	return this.put(`management/cost/${id}/`, form).then(this.response)
  }

  remove(id) {
  	return this.delete(`management/cost/${id}/`).then(this.response)
  }
}
