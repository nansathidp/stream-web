import {Base} from "./shares";

export default class extends Base {
  async find(params = {}) {
    return await this.get(`episode/`, params).then(this.response);
  }

  findById(id) {
    return this.get(`episode/${id}/`).then(this.response);
  }

  updateById(id, body) {

    return this.put(`episode/${id}/`, body)
  }

  add(title) {
    return this.post(`episode/`, title).then(this.response);
  }

  remove(id) {
    return this.delete(`episode/${id}/`,).then(this.response);
  }

  findAsset(params={}) {
    return this.get(`episode/asset/`,params).then(this.response);
  }

  findAssetById(id) {
    return this.get(`episode/asset/${id}`,).then(this.response);
  }
}
