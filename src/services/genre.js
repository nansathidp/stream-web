import {Base} from "./shares";

export default class extends Base {
  async find(params = {}) {
    return await this.get(`genre/`, params).then(this.response);
  }

  read(params = {}) {
  	return this.get(`management/genre/`, params).then(this.response)
  }

  add(form) {
  	return this.post(`management/genre/`, form).then(this.response)
  }

  update(form, id) {
  	return this.put(`management/genre/${id}/`, form).then(this.response)
  }

  remove(id) {
  	return this.delete(`management/genre/${id}/`).then(this.response)
  }
}
