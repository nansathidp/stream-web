import {Base} from "./shares";

export default class extends Base {
  find(params = {}) {
    return this.get(`deal/`, params).then(this.response);
  }

  findById(id) {
    return this.get(`deal/${id}/`).then(this.response);
  }

  updateById(id, body) {

    return this.put(`deal/${id}/`, body)
  }

  add(title) {
    return this.post(`deal/`, title).then(this.response);
  }

  remove(id) {
    // return this.delete(`title/${id}/`,).then(this.response);
    return this.delete(`deal/${id}/`,).then(this.response);
  }
}
