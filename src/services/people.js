import {Base} from "./shares";

export default class extends Base {
  async read(params = {}) {
    return await this.get(`management/people/`, params).then(this.response)
  }

  async find(params = {}) {
    return await this.get(`people/`, params).then(this.response);
  }

  async findById(id) {
    return await this.get(`people/${id}/`).then(this.response)
  }

  async add(people){
    return this.post(`management/people/`, people).then(this.response);
  }

  async update(people, id){
    return this.put(`management/people/${id}/`, people).then(this.response)
  }

  async remove(id){
    return this.delete(`management/people/${id}/`).then(this.response)   
  }

  async readaka(id) {
    return await this.get(`management/people/${id}/aka/`).then(this.response)
  }

  async readlanguage(params = {}) {
    return await this.get(`language/?is_aka=Y`, params).then(this.response)
  }

  async addaka(form, id){
    return this.post(`management/people/${id}/aka/`, form).then(this.response)
  }

  async updateaka(form) {
    return this.put(`people/aka/`, form).then(this.response)
  }
}
