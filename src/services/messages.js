import {Base} from "./shares";

export default class extends Base {
  async getAllMessage() {
    return await this.get('/scroll/message/');
  }

  async addMessage(message) {
    return await this.post('/scroll/message/', message)
  }

  async getByIDMessage(_id) {
    return await this.get(`/scroll/message/${_id}/`)
  }

  async updateMessage(_id, message) {
    return await this.put(`/scroll/message/${_id}/`, message)
  }
  async removeMessage(_id){
    return await this.delete(`/scroll/message/${_id}/`)
  }
  async sendMessage(_id, channel){
    console.log(_id, channel);
    return await this.post(`/scroll/message/${_id}/send/`, {channel})
  }

}
