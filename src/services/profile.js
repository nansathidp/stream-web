import {Token} from "./token";

export default class extends Token {
   findByToken(token) {
    return this.get(`account/profile/`);
  }
  getPermission(){
    return this.get(`account/profile/`).then(({data}) => data)
      .then(({has_permission}) => Array.isArray(has_permission) ? has_permission : []);
  }
  findGroupById() {
    return this.get(`account/group/`);
  }
  findPermission(code){
    return this.get(`account/${code}/allow/`);
  }
}
