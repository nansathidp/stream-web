import {Base} from "./shares";

export default class extends Base {
  find() {
    return this.get(`vendor/`).then(this.response);
  }
  findStudioByVendor(vendor_id) {
    return this.get(`vendor/${vendor_id}/studio/`)
  }
  async read(params = {}) {
    return await this.get(`management/vendor/`, params).then(this.response)
  }

  async add(form){
    return this.post(`management/vendor/`, form).then(this.response);
  }

  async update(form, id){
    return this.put(`management/vendor/${id}/`, form).then(this.response)
  }

  async remove(id){
    return this.delete(`management/vendor/${id}`,).then(this.response)
  }

  async readstudio(id) {
    return await this.get(`management/vendor/${id}/studio/`).then(this.response)
  }
}
