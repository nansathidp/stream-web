import {Base} from "./shares";

export default class extends Base{
  find(params) {
    return this.get('/language/', params).then(this.response)
  }

  read(params = {}) {
  	return this.get(`management/language/`, params).then(this.response)
  }

  add(form) {
  	return this.post(`management/language/`, form).then(this.response)
  }

  update(form, id) {
  	return this.put(`management/language/${id}/`, form).then(this.response)
  }

  remove(id) {
  	return this.delete(`management/language/${id}/`).then(this.response)
  }
}
