import {Base} from "./shares";

export default class extends Base {
  async find(params = {}) {
    return await this.get(`country/`, params).then(this.response);
  }

  async findById(code) {
    return await this.get(`country/${code}/`).then(this.response);
  }

  read(params = {}) {
  	return this.get(`management/country/`, params).then(this.response)
  }

  add(form) {
  	return this.post(`management/country/`, form).then(this.response)
  }

  update(form, id) {
  	return this.put(`management/country/${id}/`, form).then(this.response)
  }

  remove(id) {
  	return this.delete(`management/country/${id}/`).then(this.response)
  }
}
