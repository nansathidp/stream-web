import {Base} from "./shares";

export default class extends Base {
  findByTitleType() {
    return this.get('config/TITLE_TYPE/').then(this.response);
  }

  findByTitleStatus() {
    return this.get('config/TITLE_STATUS/').then(this.response);
  }

  findByLocalization() {
    return this.get(`config/LOCALIZE_TYPE/`).then(this.response);
  }

  findByTagThumbnail() {
    return this.get(`config/THUMNAIL_TAG/`).then(this.response);
  }
}
