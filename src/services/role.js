import {Base} from "./shares";

export default class extends Base {
  find(params = {}) {
    return this.get(`role/`, params).then(this.response)
  }

  async findById(id) {
    return await this.get(`role/${id}/`);
  }

  async read(params = {}) {
		return await this.get(`management/role/`, params).then(this.response)
	}  

  async add(form){
  	return this.post(`management/role/`, form).then(this.response);
  }

  async update(form, id){
  	return this.put(`management/role/${id}/`, form).then(this.response)
  }

  async remove(id){
  	return this.delete(`management/role/${id}/`).then(this.response)  	
  }
  
	async readaka(id) {
		return this.get(`management/role/${id}/aka/`).then(this.response)
	}

	async addaka(form, id){
  	return this.post(`management/role/${id}/aka/`, form).then(this.response)
  }
}
