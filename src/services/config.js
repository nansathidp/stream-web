import {Base} from "./shares";

export default class extends Base {
  async getAllConfig() {
    return await this.get('/scroll/scrollConfig/');
  }

  async addConfig(config) {
    return await this.post('/scroll/scrollConfig/', config)
  }

  async getByIDConfig(_id) {
    return await this.get(`/scroll/config/${_id}/`)
  }

  async updateConfig(_id, config) {
    return await this.put(`/scroll/config/${_id}/`, config)
  }
  async removeConfig(_id){
    return await this.delete(`/scroll/config/${_id}/`)
  }

}
