import {Base} from "./shares";

export default class extends Base {
  find(params = {}) {
    return this.get(`publish/`, params).then(this.response);
  }

  findById(id) {
    return this.get(`publish/${id}/`).then(this.response);
  }

  updateById(id, body) {
    return this.put(`publish/${id}/`, body).then(this.response)
  }

  add(title) {
    return this.post(`publish/`, title).then(this.response);
  }

  addPublishToolsById(body) {
    return this.post(`publish/tools/`, body).then(this.response)
  }

  remove(id) {
    return this.delete(`publish/${id}/`,).then(this.response);
  }

  schedule(params = {}) {
    return this.post(`publish/schedule/`, params).then(this.response)
  }

  // update by danai 

  async read(params = {}) {
    return await this.get(`management/publish/`, params).then(this.response)
  }

  async readbyid(form) {
    return await this.get(`management/publish/getbyid/`, form).then(this.response)
  }

  async add_publish(from){
    return this.post(`management/publish/`, from).then(this.response);
  }

  async update(from, id){
    return this.put(`management/publish/${id}/`, from).then(this.response)
  }

  async remove(id){
    return this.delete(`management/publish/${id}/`).then(this.response)   
  }

  async readlanguage(params = {}) {
    return await this.get(`language/?is_localize=Y`, params).then(this.response)
  }

  async readaudio(id){
    return this.get(`management/publish/${id}/audio/`).then(this.response)
  }

  async readsubtitle(id){
    return this.get(`management/publish/${id}/subtitle/`).then(this.response)
  }

  async readvideo(id){
    return this.get(`management/publish/${id}/video/`).then(this.response)
  }

  async readcountry(id){
    return this.get(`management/publish/${id}/country/`).then(this.response)
  }

  async readdirectory(id) {
    return this.get(`management/publish/${id}/directory/`).then(this.response)
  }

}
