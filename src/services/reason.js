import {Base} from "./shares";

export default class extends Base {
  async findById(id) {
    return await this.get(`reason/${id}/`).then(this.response);
  }

  async find(params = {}) {
    return await this.get(`reason/`, params).then(this.response)
}
}