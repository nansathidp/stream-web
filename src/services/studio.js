import {Base} from "./shares";

export default class extends Base {
  async find(params = {}) {
    return await this.get(`studio/`, params = {}).then(this.response);
  }

  async read(params = {}) {
		return await this.get(`management/studio/`, params).then(this.response)
	}

	async add(form){
  	return this.post(`management/studio/`, form).then(this.response);
  }

  async update(form, id){
  	return this.put(`management/studio/${id}/`, form).then(this.response)
  }

  async remove(id){
  	return this.delete(`management/studio/${id}/`).then(this.response)
  }

	async readaka(id) {
		return await this.get(`management/studio/${id}/aka/`).then(this.response)
	}

	async addaka(form, id){
  	return this.post(`management/studio/${id}/aka/`, form).then(this.response)
  }
}
