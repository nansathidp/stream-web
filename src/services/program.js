import {Base} from "./shares";

export default class extends Base {
  async getAllContent(params) {
    return await this.get('program/content/', params);
  }

  addContent(content) {
    return this.post('program/content/', content);
  }

  async getByIdContent(id) {
    return await this.get(`program/content/${id}/`);
  }

  searchContent(search) {
    return this.get('program/content/?search=' + search);
  }

  updateContent(id, content) {
    return this.put(`program/content/${id}/`, content);

  }

  removeContent(id) {
    return this.delete(`program/content/${id}/`);
  }

  getAllSeries(params) {
    return this.get('program/series/', params);
  }

  searchSeries(search) {
    return this.get('program/series/?search=' + search);
  }

  CreateContentList(data) {
    return this.post('program/bulk/content/', data)
  }

  addSeries(series) {
    return this.post('program/series/', series)
  }

  getByIdSeries(id) {
    return this.get(`program/series/${id}/`)
  }

  updateSeries(id, series) {
    return this.put(`program/series/${id}/`, series)
  }

  removeSeries(id) {
    return this.delete(`program/series/${id}/`)
  }


}
