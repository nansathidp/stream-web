import {Base} from "./shares";

export default class extends Base {
  find() {
    return this.get(`other-type/`).then(this.response);
  }

  read(params = {}) {
  	return this.get(`management/other-type/`, params).then(this.response)
  }

  add(form) {
  	return this.post(`management/other-type/`, form).then(this.response)
  }

  update(form, id) {
  	return this.put(`management/other-type/${id}/`, form).then(this.response)
  }

  remove(id) {
  	return this.delete(`management/other-type/${id}/`).then(this.response)
  }
}
