import {Base} from "./shares";

export default class extends Base {
  find(params = {}) {
    return this.get(`title/`, params).then(this.response);
  }

  findById(id) {
    return this.get(`title/${id}/`).then(this.response);
  }

  importCSV(body) {
    return this.post(`title/import_file/`, body).then(this.response);
  }

  updateById(id, body) {
    return this.put(`title/${id}/`, body).then(this.response)
  }

  validatePeriod(id, body) {
    return this.put(`title/${id}/validated_period/`, body).then(this.response)
  }

  add(title) {
    return this.post(`title/`, title).then(this.response);
  }

  addTitleMini(title) {
    return this.post(`title/mini/`, title).then(this.response);
  }

  getTitleMini(id) {
    return this.get(`title/mini/${id}/`).then(this.response);
  }

  remove(id) {
    // return this.delete(`title/${id}/`,).then(this.response);
    return this.delete(`title/${id}/`,).then(this.response);
  }

  downloadFormat() {
    return this.get(`title/download_format/`,).then(this.response);
  }

  syncTitle(title) {
    return this.post(`title/sync/`, title).then(this.response);
  }
}
