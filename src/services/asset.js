import {Base} from "./shares";

export default class extends Base {
  find(params = {}) {
    return this.get(`asset/`, params).then(this.response)
  }

  async findByid(id) {
    return await this.get(`asset/${id}/`).then(this.response)
  }

  async changeLanguage(asset_id, info) {
    return await this.put(`asset/${asset_id}/`, info).then(this.response)
  }
  // async getInfo() {
  //   return await this.get(`/asset/info`);
  // }
  // async updateLocal(id, local) {
  //   return await this.put(`/asset/info/${id}/local`, local);
  // }
  // async updateAllLocal(local) {
  //   return await this.put(`/asset/info/local`, local);
  // }
  // async updateAllSyncCms(cms) {
  //   return await this.put(`/asset/info/sync`, cms);
  // }
  // async deleteInfo(id) {
  //   return await this.delete(`/asset/info/${id}/`);
  // }
  // async searchInfo(search) {
  //   return await this.get(`/asset/info/?search=` + search);
  // }
  // async paginatorInfo(page_size, page) {
  //   return await this.get(`/asset/info/?page_size=${page_size}&page=${page}`);
  // }
  // async getByIdInfo(id) {
  //   return await this.get(`/asset/info/${id}/`);
  // }
  // async updateInfo(id, info) {
  //   return await this.put(`/asset/info/${id}/`, info);
  // }
  // async getCategory() {
  //   return await this.get(`/asset/category`);
  // }
  // async getByCodeCategory(code) {
  //   return await this.get(`/asset/category/${code}/`);
  // }
  // async addCategory(category) {
  //   return await this.post(`/asset/category/`, category);
  // }
  // async addCategoryType(code, category) {
  //   return await this.post(`/asset/category/${code}/type`, category);
  // }
  // async updateCategory(code, category) {
  //   return await this.put(`/asset/category/${code}/`, category);
  // }
  // async updateCategoryType(code, category) {
  //   return await this.put(`/asset/category/${code}/type`, category);
  // }
  // async deleteCategory(code) {
  //   return await this.delete(`/asset/category/${code}/`);
  // }
  // async deleteCategoryType(id, type) {
  //   return await this.delete(`/asset/category/${id}/type/${type}`);
  // }

  // async addNode(node) {
  //   return await this.post(`/asset/node/`, node);
  // }
  // async getNode() {
  //   return await this.get(`/asset/node`);
  // }
  // async getByIdNode(id) {
  //   return await this.get(`/asset/node/${id}/`);
  // }
  // async deleteNode(id, node) {
  //   return await this.delete(`/asset/node/${id}/`, node);
  // }
  // async updateNode(id, node) {
  //   return await this.put(`/asset/node/${id}/`, node);
  // }
  // async addPath(path) {
  //   return await this.post(`/asset/path/`, path);
  // }
  // async getPath(id, path) {
  //   return await this.get(`/asset/node/${id}/path?action=` + path);
  // }
  // async getPathById(pathId) {
  //   return await this.get(`/asset/path/${pathId}`);
  // }
  // async updatePathById(pathId, path) {
  //   return await this.put(`/asset/path/${pathId}/`, path);
  // }
  // async deletePathById(pathId) {
  //   return await this.delete(`/asset/path/${pathId}`);
  // }
}
