import {Base} from "./shares";

export default class extends Base {
  findByVodType() {
    return this.get('config/VOD_TYPE/').then(this.response);
  }

  findByDealStatus() {
    return this.get('config/DEAL_STATUS/').then(this.response);
  }
}
