import {Base} from "./shares";

export default class extends Base {
  async find(params) {
    return await this.get(`dashboard/deal/contract/`, params).then(this.response);
  }

  async export(params) {
    return await this.post(`/dashboard/deal/contract/export/`, params).then(this.response);
  }

  async find_month(params) {
    return await this.get(`dashboard/deal/contract/find_month/`, params).then(this.response);
  }


}
